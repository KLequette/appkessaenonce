
package progsconsole;

import entites.Technicien;
import static utilitaires.UtilDate.format;


public class TestMethodesTechnicien {

  
    public static void main(String[] args) {
        
        Technicien tech= Technicien.getLeTechnicienNumero(802L);
        
        System.out.println("\nTest de la méthode coût horaire\n");
        
        System.out.println("Technicien N°: "+tech.getNumero());
        System.out.println("Nom :"+tech.getNom());
        System.out.println("Grade :"+tech.getLeGrade().getLibelle());
        System.out.println("Taux horaire afférent au grade : "+tech.getLeGrade().getTauxHoraire()+" €");
        System.out.println("Embauché(e) le "+format(tech.getDateEmbauche()));
        System.out.println("Coût majoration :"+tech.coutHoraireTechnicien()+ " €\n");
    }
}
